#!/usr/bin/env python

#Read the serial output and print it to screen.
#Author: Francesco Guescini.

#******************************************
#import stuff
import time, sys
import json
import influxdb
from influxdb import InfluxDBClient
import math
#******************************************

def writeInflux(meas,fields):

    #------------------------------------------

    logfile = open('logfile.csv', 'w')
    logfile.write('# Time, temp, humidity, dustcount, ntc0, ntc1, ntc2, ntc3\n')
    host = '128.3.50.38'
    port = '8086'
    dbname = 'xray'
    dbuser = 'xray_rw'
    dbuser_password = '9j9ICysgLkBL'
    query = 'XRM'
    client = InfluxDBClient(host=host, port=port, database=dbname, username=dbuser, password=dbuser_password)

    print("Writing")
    json_body = [
        {
            "measurement": meas,
            "fields": fields,
        }
    ]    
    client.write_points(json_body)


#******************************************
if __name__ == "__main__":
    writeInflux("peltier",{"T":23})
    print()