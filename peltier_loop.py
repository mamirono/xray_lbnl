import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
import matplotlib.pyplot as plt
from util import *
from influx import writeInflux




def adjust_peltier(target):
    max_v=10.0
    max_i=6.0

    i=ps_peltier_measI()
    v=ps_peltier_measV()
    t_ntc=meas_temp()
    writeInflux("peltier",{"I":i, "V":v, "T_NTC":t_ntc})

    if t_ntc > target+0.5: 
        print("cooling")
        #too hot, need to cool more
        new_v=round(v+0.1,1)
        print(v,new_v,i)
        if new_v<max_v and i<max_i:
            ps_peltier_setV(new_v)
            time.sleep(1)
        else:
            print("Voltage %s or current %s too high, not increasing!" %(new_v,i))

    if t_ntc < target-0.5: 
        print("heating")
        #too cold, need to cool less
        new_v=round(v-0.1,1)
        if new_v>0.5:
            ps_peltier_setV(new_v)
            time.sleep(1)
        else:
            print("Voltage %s too low, not decreasing!" %(new_v))

    time.sleep(5)
  