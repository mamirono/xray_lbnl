import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
from util import *

outname="SLDO_m60_LP_fine.txt"



outfile = open(outname,"a")

names=["Time","Temp","V_PS","I_PS","VinA_DMM","VinD_DMM","VDDA_DMM","VDDD_DMM","Vofs_DMM","Vref_Core","Vref_PRE"]

for name in names:
    outfile.write(str(name)+"\t")

outfile.write(str(name)+"\n")

currents=np.arange(4,0,-0.1)

chip_config="/home/mmironova/YARR_ITkPixV2/configs/connectivity/itkpixv2_test.json"

ps_setI(4,currents[0])
ps_setV(4,2.5)
ps_on(4)
os.system("./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup_LP.json -s configs/scans/itkpixv2/std_digitalscan.json -p")

for current in currents:
	ps_setI(4,current)	
	time.sleep(1)
	output=[]
	outfile = open(outname,"a")
	outfile.write(str(time.time())+"\t")

	#measure T 
	temp=meas_temp()
	outfile.write(str(temp)+"\t")

	v=ps_measV(4)
	outfile.write(str(v)+"\t")

	i=ps_measI(4)
	outfile.write(str(i)+"\t")

	# Measure VinA, VinD, VDDA, VDDD from DMM
	for i in [1,2,3,4,7]:
		v=meas_dmm(i)
		outfile.write(str(v)+"\t")

	for vmux in [31,32]:
		v=meas_vmux(vmux)
		outfile.write(str(v)+"\t")

	outfile.write("\n")
	outfile.close()
