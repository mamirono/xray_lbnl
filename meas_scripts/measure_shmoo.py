import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
import matplotlib.pyplot as plt
from util import *


# measure
filename="Shmoo_m50.txt"

outfile = open(filename,"a")
currents=np.arange(0.5,3.2,0.1)


names=["Time","Temp","VDDA_Trim","VDDD_Trim","VDDA","VDDD","Eye"]

for name in names:
    outfile.write(str(name)+"\t")

outfile.write(str(name)+"\n")
outfile.close()

VDDA_trims=range(0,16,1)
VDDD_trims=range(0,16,1)

for VDDA in VDDA_trims:
    for VDDD in VDDD_trims: 
        outfile = open(filename,"a")
        update_config("SldoTrimA",VDDA)
        update_config("SldoTrimD",VDDD)
        configure()

        time.sleep(1)

        outfile.write(str(time.time())+"\t")
        #measure T 
        temp=meas_temp()
        outfile.write(str(temp)+"\t")
        
        outfile.write(str(VDDA)+"\t")
        outfile.write(str(VDDD)+"\t")


        for i in [3,4]:
            v=meas_dmm(i)
            outfile.write(str(v)+"\t")

        proc = subprocess.Popen(shlex.split("./bin/linkQuality -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json"),stdout=subprocess.PIPE)
        output=str(proc.stdout.read().strip().decode())
        print(output.split("\n")[-1])
    
        for i in range(0,5):
            if i==0:
                outfile.write(output.split("|")[0].split("\n")[-1])
            else:
                outfile.write(output.split("|")[i]+"\t")


        outfile.write("\n")

        outfile.close()
 
        #os.system()


'''
    for current in currents: 
        ps_off(4)
        ps_setV(4,2.5)
        ps_setI(4,current)
        ps_on(4)

        outfile = open("Cooldown_SLDO2.txt","a")
        outfile.write(str(time.time())+"\t")
        #measure T 
        temp=meas_temp()
        outfile.write(str(temp)+"\t")

        v=ps_measV(4)
        outfile.write(str(v)+"\t")

        i=ps_measI(4)
        outfile.write(str(i)+"\t")
        # Measure VinA, VinD, VDDA, VDDD from DMM
        for i in [1,2,3,4]:
            v=meas_dmm(i)
            outfile.write(str(v)+"\t")

        for vmux in [0,31,32,33,34,35,36,37,38,39]:
            v=meas_vmux(vmux)
            outfile.write(str(v)+"\t")

        for imux in [0,28,29,30,31]:
            i=meas_imux(imux)
            outfile.write(str(i)+"\t")

        outfile.write("\n")
        outfile.close()
'''