import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
import matplotlib.pyplot as plt
from util import *


# measure

outfile = open("Cooldown_SLDO2.txt","a")
currents=np.arange(0.5,3.2,0.1)


names=["Time","Temp","V_PS","I_PS","VinA_DMM","VinD_DMM","VDDA_DMM","VDDD_DMM","Vref_ADC","Vref_Core","Vref_PRE","VinA4","VDDA2","VrefA","Vofs4","VinD4","VDDD2","VrefD","Iref","IinA","IshuntA","IinD","IshuntD"]

for name in names:
    outfile.write(str(name)+"\t")

outfile.write(str(name)+"\n")
outfile.close()

for i in range (0,10000000):
    for current in currents: 
        ps_off(4)
        ps_setV(4,2.5)
        ps_setI(4,current)
        ps_on(4)

        outfile = open("Cooldown_SLDO2.txt","a")
        outfile.write(str(time.time())+"\t")
        #measure T 
        temp=meas_temp()
        outfile.write(str(temp)+"\t")

        v=ps_measV(4)
        outfile.write(str(v)+"\t")

        i=ps_measI(4)
        outfile.write(str(i)+"\t")
        # Measure VinA, VinD, VDDA, VDDD from DMM
        for i in [1,2,3,4]:
            v=meas_dmm(i)
            outfile.write(str(v)+"\t")

        for vmux in [0,31,32,33,34,35,36,37,38,39]:
            v=meas_vmux(vmux)
            outfile.write(str(v)+"\t")

        for imux in [0,28,29,30,31]:
            i=meas_imux(imux)
            outfile.write(str(i)+"\t")

        outfile.write("\n")
        outfile.close()
