import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
import matplotlib.pyplot as plt
from util import *
from influx import writeInflux
from peltier_loop import adjust_peltier

directory="irradiation_3108/"

if not os.path.exists(directory):
	os.makedirs(directory)

vmux_idx=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,30,31,32,33,34,35,36,37,38,39]
imux_idx=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,63]

while True:
    try:

        T_env,H_env=check_T_humidity()
        writeInflux("Env",{"T":T_env,"H":H_env})

        adjust_peltier(-10)

        results={}
        
        results["time"]=time.time()
        results["Env"]={"T":T_env,"H":H_env}

        v_ps_1=ps_measV(1)
        i_ps_1=ps_measI(1)
        v_ps_2=ps_measV(2)
        i_ps_2=ps_measI(2)
        writeInflux("PS",{"v_ps_1":v_ps_1,
                        "i_ps_1":i_ps_1,
                        "v_ps_2":v_ps_2,
                        "i_ps_2":i_ps_2})
        results["PS"]={"v_ps_1":v_ps_1,
                        "i_ps_1":i_ps_1,
                        "v_ps_2":v_ps_2,
                        "i_ps_2":i_ps_2}


        hv_status, shutter_status, xray_voltage, xray_current, xray_time = check_xray_status()
        writeInflux("Xray",{"hv_status":hv_status,"shutter_status":shutter_status, "voltage":xray_voltage, "current": xray_current, "exp_time":xray_time})
        results["Xray"]={"hv_status":hv_status,"shutter_status":shutter_status, "voltage":xray_voltage, "current": xray_current, "exp_time":xray_time}

        t_ntc=meas_temp()
        print(t_ntc)

        results["T"]=t_ntc

        for val in ["VinA_DMM","VinD_DMM","VDDA_DMM","VDDD_DMM","Vref_pre_DMM"]:
            v=meas_dmm(mapping[val])
            writeInflux("ITkPixV2",{val: v})
            results[val]=v

        for vmux in vmux_idx:
            v=meas_vmux(vmux)
            writeInflux("ITkPixV2",{vmux_map[vmux]:v})   
            results[vmux_map[vmux]]=v

        for imux in imux_idx:
            i=meas_imux(imux)
            writeInflux("ITkPixV2",{imux_map[imux]:i})  
            results[imux_map[imux]]=i 
        
        vddd_val=meas_dmm(mapping["VDDD_DMM"])
        time.sleep(1)
        rosc=meas_rosc()
        results["ROSC"]={"VDDD":vddd_val,"ROSC":rosc}    
        writeInflux("ITkPixV2",{"VDDD_ROSC":vddd_val})  
        for i in range(0,len(rosc)):
            print("ITkPixV2",{"ROSC%s"%i:float(rosc[i])})
            writeInflux("ITkPixV2",{"ROSC%s"%i:float(rosc[i])})  



        outname="meas_"+str(round(time.time()))+".json"
        with open(directory+outname, "w") as jsonFile:
	        json.dump(results, jsonFile, indent=2)	        
        
    
        

    except KeyboardInterrupt:
        print()
        sys.exit(0)          