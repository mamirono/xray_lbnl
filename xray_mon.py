#!/usr/bin/env python

#Read the serial output and print it to screen.
#Author: Francesco Guescini.

#******************************************
#import stuff
import argparse, serial, time, sys
import json
import influxdb
from influxdb import InfluxDBClient
import math
#******************************************



def readSerial(args):
    """Read the serial output and print it to screen."""

    #------------------------------------------
    #serial setup
    xray = serial.Serial(
        port = args.port,
        baudrate = args.baudrate,
        parity = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        timeout = 60)

    print(xray.isOpen())

    xray.write(b'SR:03')
    print(xray.readall())
    xray.close()


#******************************************
if __name__ == "__main__":

    #------------------------------------------
    #parse input arguments
    parser = argparse.ArgumentParser(description="%prog [options]")
    parser.add_argument("-p", "--port", dest="port", default="/dev/ttyUSB3", help="serial port, usually \"/dev/cu.SLAB_USBtoUART\" or \"/dev/ttyUSB0\"")
    parser.add_argument("-b", "--baudrate", dest="baudrate", default=9600, help="baud rate")
    args = parser.parse_args()
    
    #------------------------------------------
    #read serial
    readSerial(args)
    print()
