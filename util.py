import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
import matplotlib.pyplot as plt

with open("paths.json", "r") as jsonFile:
	paths = json.load(jsonFile)

chip_config=paths["chip_config"]
connectivity_config=paths["connectivity_config"]
spec_config=paths["spec_config"]

labRemote_path=paths["labRemote_path"]
labRemote_config=paths["labRemote_config"]

yarr_path=paths["yarr_path"]


mapping={
	"VinA_DMM":1,
	"VinD_DMM":2,
	"VDDA_DMM":3,
	"VDDD_DMM":4,
	"VMUX":5,
	"IMUX":6,
	"Vref_pre_DMM":7,
	"NTC":8
}

Rmux=4990

# PS stuff
ps_cmd="%s/build/bin/powersupply -e %s -n PS_chip" %(labRemote_path,labRemote_config)

def ps_on(channel):
	os.system("%s -c %s power-on" %(ps_cmd,channel))

def ps_off(channel):
	os.system("%s -c %s power-off" %(ps_cmd,channel))

def ps_setV(channel,val):
	os.system("%s -c %s set-voltage %s" %(ps_cmd,channel,val))

def ps_setI(channel,val):
	os.system("%s -c %s set-current %s" %(ps_cmd,channel,val))

def ps_measI(channel):
	proc = subprocess.Popen(shlex.split("%s -c %s meas-current" %(ps_cmd,channel)), stdout=subprocess.PIPE)
	meas = float(proc.stdout.read().strip())	
	return meas

def ps_measV(channel):
	proc = subprocess.Popen(shlex.split("%s -c %s meas-voltage" %(ps_cmd,channel)), stdout=subprocess.PIPE)
	meas = float(proc.stdout.read().strip())	
	return meas

#DMM stuff 
dmm_cmd="%s/build/bin/meter -e %s" %(labRemote_path,labRemote_config)

def meas_dmm(channel):
	proc = subprocess.Popen(shlex.split("%s -c %s -n Meter meas-voltage"%(dmm_cmd,channel)), stdout=subprocess.PIPE)
	val = float(proc.stdout.read().strip())	
	return val

def meas_dmm_R(channel):
	proc = subprocess.Popen(shlex.split("%s -c %s -n Meter meas-res"%(dmm_cmd,channel)), stdout=subprocess.PIPE)
	val = float(proc.stdout.read().strip())	
	return val

def write_register(name,val):
	os.system('%s/bin/write-named-register -r %s -c %s -n %s -v %s'%(yarr_path,spec_config,connectivity_config,name,val))

def meas_vmux(vmux_sel):
	write_register("MonitorEnable",1)
	write_register("MonitorV",vmux_sel)
	time.sleep(1)
	val = meas_dmm(mapping["VMUX"])
	#write_register("MonitorV",30)
	#time.sleep(1)
	#gnd = meas_dmm(mapping["VMUX"])
	return(val)

def meas_imux(imux_sel):
	write_register("MonitorEnable",1)
	write_register("MonitorI",imux_sel)
	time.sleep(1)
	val = meas_dmm(mapping["IMUX"])
	#write_register("MonitorI",63)
	#time.sleep(1)
	#gnd = meas_dmm(mapping["IMUX"])
	val=val/Rmux
	return(val)	

def meas_temp():
	NtcCalPar = [
		0.0007488999981433153,
		0.0002769000129774213,
		7.059500006789676e-08,
	]
	A = NtcCalPar[0]
	B = NtcCalPar[1]
	C = NtcCalPar[2]
	Rntc = meas_dmm_R(mapping["NTC"])
	temp = 1 / (A + B * np.log(Rntc) + C * ((np.log(Rntc)) ** 3)) - 273.15
	return temp


def update_config(name,val):
	with open("%s"%chip_config, "r") as jsonFile:
		data = json.load(jsonFile)
	data['ITKPIXV2']['GlobalConfig'][name]=val
	with open("%s"%chip_config, "w") as jsonFile:
		json.dump(data, jsonFile, indent=4)	

def configure():
	os.system("%s/bin/scanConsole -r %s -c %s"%(yarr_path,spec_config,connectivity_config))



# Peltier stuff 
ps_peltier_cmd="%s/build/bin/powersupply -e %s -n PS_peltier -c 1" %(labRemote_path,labRemote_config)

def ps_peltier_on():
	os.system("%s power-on" %(ps_peltier_cmd))

def ps_peltier_off():
	os.system("%s power-off" %(ps_peltier_cmd))

def ps_peltier_setV(val):
	if val>10.0:
		print("Set value larger than 5 V, not setting anything")
	else:
		print("Setting %s"%val)
		os.system("%s ramp-voltage %s 1" %(ps_peltier_cmd,val))
		time.sleep(1)
		set_val=ps_peltier_measV()
		print("Done %s" %set_val)


def ps_peltier_setI(val):
	if val>6.0:
		print("Set value larger than 5 A, not setting anything")
	else:
		os.system("%s set-current %s" %(ps_peltier_cmd,val))

def ps_peltier_measI():
	proc = subprocess.Popen(shlex.split("%s meas-current" %(ps_peltier_cmd)), stdout=subprocess.PIPE)
	meas = float(proc.stdout.read().strip())	
	return meas

def ps_peltier_measV():
	print("%s meas-voltage" %(ps_peltier_cmd))
	proc = subprocess.Popen(shlex.split("%s meas-voltage" %(ps_peltier_cmd)), stdout=subprocess.PIPE)
	meas = float(proc.stdout.read().strip())	
	return meas


# Scan stuff 

def meas_rosc():
	proc = subprocess.Popen(shlex.split("%s/bin/Itkpixv2ReadRingosc -r %s -c %s"%(yarr_path,spec_config,connectivity_config)),stdout=subprocess.PIPE)
	time.sleep(15)
	val = proc.stdout.read().strip().decode()
	rosc_vals=val.split(" ")
	return rosc_vals
	
def eye_diagram():
	os.system("%s/bin/eyeDiagram -r %s -c %s"%(yarr_path,spec_config,connectivity_config))

# X-ray stuff 

def check_xray_status():
	proc = subprocess.Popen(shlex.split("%s/build/bin/xray_status"%labRemote_path),stdout=subprocess.PIPE)
	val = proc.stdout.read().strip().decode()
	hv_status = float(val.split("\n")[0].split(":")[1])
	shutter_status = float(val.split("\n")[1].split(":")[1])
	voltage = float(val.split("\n")[2].split(":")[1])
	current = float(val.split("\n")[3].split(":")[1])
	time = float(val.split("\n")[4].split(":")[1])
	return hv_status, shutter_status, voltage, current, time

# Env stuff 

def check_T_humidity():
	proc = subprocess.Popen(shlex.split("%s/build/examples/temp_hum_monitor_example"%labRemote_path),stdout=subprocess.PIPE)
	val=proc.stdout.read().strip().decode()
	T=float(val.split("\n")[-2])
	H=float(val.split("\n")[-1])
	return T,H

vmux_map = {
        0: "GADC",
        1: "ImuxPad",
        2: "Vntc",
        3: "VcalDac",
        4: "VDDAcapmeas",
        5: "VPolSensTop",
        6: "VPolSensBottom",
        7: "VcalHi",
        8: "VcalMed",
        9: "VDiffVTH2",
        10: "VDiffVTH1Main",
        11: "VDiffVTH1Left",
        12: "VDiffVTH1Right",
        13: "VRadSensAna",
        14: "VMonSensAna",
        15: "VRadSensDig",
        16: "VMonSensDig",
        17: "VRadSensCenter",
        18: "VMonSensAcb",
        19: "AnaGND19",
        20: "AnaGND20",
        21: "AnaGND21",
        22: "AnaGND22",
        23: "AnaGND23",
        24: "AnaGND24",
        25: "AnaGND25",
        26: "AnaGND26",
        27: "AnaGND27",
        28: "AnaGND28",
        29: "AnaGND29",
        30: "AnaGND30",
        31: "VrefCore",
        32: "VrefOVP",
        33: "VinA",
        34: "VDDA",
        35: "VrefA",
        36: "Vofs",
        37: "VinD",
        38: "VDDD",
        39: "VrefD",
        63: "HighZ"
    }

imux_map = {
        0: "Iref",
        1: "CdrVcoMainBias",
        2: "CdrVcoBuffBias",
        3: "ICdrCP",
        4: "ICdrFD",
        5: "CdrBuffBias",
        6: "CMLDrivTap2Bias",
        7: "CMLDrivTap1Bias",
        8: "CMLDrivMainBias",
        9: "Intc",
        10: "CapMeas",
        11: "CapMeasPar",
        12: "IDiffPreMain",
        13: "IDiffPreampComp",
        14: "IDiffComp",
        15: "IDiffVth2",
        16: "IDiffVth1Main",
        17: "IDiffLcc",
        18: "IDiffFB",
        19: "IDiffPreampLeft",
        20: "IDiffVth1Left",
        21: "IDiffPreampRight",
        22: "IDiffPreampTopLeft",
        23: "IDiffVth1Right",
        24: "IDiffPreampTop",
        25: "IDiffPreampTopRight",
        26: "NotUsed26",
        27: "NotUsed27",
        28: "IinA",
        29: "IshuntA",
        30: "IinD",
        31: "IshuntD",
        63: "HighZ"
    }


ms=[704.7765857058042, 1071.0357499354805, 703.3917327058083, 1079.903213380085, 773.3610220827634, 1311.3291508069083, 917.8609162697186, 1346.9569752831899, 1098.3537736897754, 1099.7324057227247, 1265.7556798729565, 1268.445365860295, 1120.706372095997, 1124.3738478223709, 1275.64710772158, 1273.4352863174452, 1305.6854205522789, 1308.0940827995394, 1538.0960012501182, 1528.0100223753905, 1287.986765526399, 1292.10398511577, 1592.803839422063, 1596.1933396952695, 788.794662782081, 792.8265696790935, 952.7740955001236, 958.9033233481132, 953.9446866780514, 953.287638939998, 1067.6476872961857, 1109.5164282733158, 1074.1807128636915, 1119.2968014538699, 1476.5058793876751, 1476.5655734229126, 1475.9291047778947, 1482.0804647369387, 1480.5731568295694, 1477.3446273829925, 1477.0716619971156, 1476.2787399726412]
bs=[-396.6817019135441, -593.2342105855923, -406.2546801476185, -612.908077378153, -508.54810389676516, -853.1737057483022, -639.344271961856, -915.0350544260124, -635.0708334523526, -642.260966110529, -732.4905034476188, -737.041544053642, -673.0728443941172, -679.9756466416084, -754.3319839920656, -747.2320436226977, -865.6907843377626, -874.8102902702518, -1011.2375965776557, -1008.3263227821643, -895.3422056141513, -892.0144194182016, -1084.2758447357087, -1091.1840736136278, -490.7842639808509, -499.9215474785959, -606.1628215186784, -610.667605394466, -598.0496479080066, -591.4529049697834, -489.36938651013656, -486.54029052334397, -565.4319502435415, -585.8819094864019, -977.3347752922109, -974.150114492849, -979.8829111390093, -976.7363447481049, -978.8824728119054, -973.4894021252416, -977.8979062596821, -976.261699850891]