import os,sys
import subprocess
import shlex
import json 
import numpy as np 
import time
from util import *

outname="ROSC_VDDD.json"
directory="EyeDiagrams_VDDD/"
if not os.path.exists(directory):
	os.makedirs(directory)
VDDDs=range(0,16,1)

results={}
for vddd in VDDDs:

	update_config("SldoTrimD",vddd)
	time.sleep(1)
	eye_diagram()
	configure()

	vddd_val=meas_dmm(mapping["VDDD_DMM"])
	time.sleep(1)
	rosc=meas_rosc()
	print(vddd_val, rosc)
	results[vddd]={"VDDD":vddd_val,"ROSC":rosc}
	os.system("cp results.txt %s/results_%s.txt" % (directory, vddd))

with open(outname, "w") as jsonFile:
	json.dump(results, jsonFile, indent=2)	